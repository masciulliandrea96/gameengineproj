// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/PlayerHUDWidget.h"
#include "Blueprint/UserWidget.h"
#include "Components/Image.h"
#include "Internationalization/Text.h"
#include "States/MarioPlayerState.h"
#include "Components/TextBlock.h"

void UPlayerHUDWidget::UpdateCoins(int newCoinAmount)
{
	mCoinsText->SetText(FText::AsNumber(newCoinAmount));
}

void UPlayerHUDWidget::UpdateLives(int newLifeAmount)
{
	// if player has all lives, set all lives images visible

	if (newLifeAmount >= 3) 
	{
		mHeartImage_3->SetColorAndOpacity(FLinearColor::White);
		mHeartImage_2->SetColorAndOpacity(FLinearColor::White);
		mHeartImage_1->SetColorAndOpacity(FLinearColor::White);
	}

	// render "hearts" images transparent when lost

	if (newLifeAmount < 3)
		mHeartImage_3->SetColorAndOpacity(FLinearColor::Transparent);

	if (newLifeAmount < 2)
		mHeartImage_2->SetColorAndOpacity(FLinearColor::Transparent);

	if (newLifeAmount < 1)
		mHeartImage_1->SetColorAndOpacity(FLinearColor::Transparent); // dead
}

void UPlayerHUDWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();
	AMarioPlayerState* pPlayerState = GetWorld()->GetFirstPlayerController()->GetPlayerState<AMarioPlayerState>();
	pPlayerState->OnCoinAmountUpdate.AddDynamic(this, &UPlayerHUDWidget::UpdateCoins);
	pPlayerState->OnLifeAmountUpdate.AddDynamic(this, &UPlayerHUDWidget::UpdateLives);
}
// Fill out your copyright notice in the Description page of Project Settings.


#include "States/MarioPlayerState.h"
#include "Character/CharacterBase.h"
//#include "GameFramework/Actor.h"
#include "CharacterBaseGameMode.h"

AMarioPlayerState::AMarioPlayerState()
{
	mCoins = 0;
	mCurrentHearts = mMaxNumHearts;
	mIsInvincible = false;
}

void AMarioPlayerState::AddCoins(int coins/* = 1 */)
{
	mCoins += coins;
	OnCoinAmountUpdate.Broadcast(mCoins);

	if (mCoins < mCoinsToWin)
		return;

	ACharacterBaseGameMode* gamemode = Cast<ACharacterBaseGameMode>(GetWorld()->GetAuthGameMode());
	if (gamemode)
		gamemode->WinLevel();
}

void AMarioPlayerState::PlayerTakesDamage(int damage /* = 1 */) 
{
	// invincibility logic

	if (mIsInvincible)
		return;

	FTimerHandle UnusedHandle;
	GetWorldTimerManager().SetTimer( UnusedHandle, this, &AMarioPlayerState::SetVincible, 2.0f, false );

	SetInvincible();

	// remove hearts

	RemoveHeart(damage);
}

void AMarioPlayerState::SetInvincible() 
{
	mIsInvincible = true;

	ACharacterBase* myPlayer = Cast<ACharacterBase>(GetPawn());
	if (myPlayer)
		myPlayer->CharacterStartBlink();

}

void AMarioPlayerState::SetVincible() 
{
	mIsInvincible = false;

	ACharacterBase* myPlayer = Cast<ACharacterBase>(GetPawn());
	if (myPlayer)
		myPlayer->CharacterStopBlink();
}

void AMarioPlayerState::RemoveHeart(int damage /* = 1*/)
{
	mCurrentHearts -= damage;

	OnLifeAmountUpdate.Broadcast(mCurrentHearts);

	if (mCurrentHearts <= 0)
		PlayerDeath();
}

void AMarioPlayerState::RestoreHearts()
{
	mCurrentHearts = mMaxNumHearts;
	OnLifeAmountUpdate.Broadcast(mCurrentHearts);
}

void AMarioPlayerState::PlayerDeath()
{
	ACharacterBase* myPlayer = Cast<ACharacterBase>(GetPawn());
	if (myPlayer)
		myPlayer->CharacterDeath();
}

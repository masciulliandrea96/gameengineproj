// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterBaseGameMode.h"

#include "Kismet/GameplayStatics.h"

ACharacterBaseGameMode::ACharacterBaseGameMode()
{

}

void ACharacterBaseGameMode::RestartGame()
{
	FTimerHandle UnusedHandle;
	GetWorldTimerManager().SetTimer( UnusedHandle, this, &ACharacterBaseGameMode::ReopenLevel, 2.0f, false );
}


void ACharacterBaseGameMode::ReopenLevel()
{
	UGameplayStatics::OpenLevel(this, FName(*GetWorld()->GetName()), false);
}

void ACharacterBaseGameMode::WinLevel() {
	
	// load game won map

	UGameplayStatics::OpenLevel(this, FName("YouWonMap"), false);

}
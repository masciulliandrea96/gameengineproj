// Fill out your copyright notice in the Description page of Project Settings.


#include "Pickups/PickupItem.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
APickupItem::APickupItem()
{
	PrimaryActorTick.bCanEverTick = true;

	mSphere = CreateDefaultSubobject<USphereComponent>(TEXT("My Item Sphere"));
	if (mSphere)
	{
		RootComponent = mSphere;
		mSphere->SetSimulatePhysics(false);
	}

	mMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("My Item Mesh"));
	if (mMesh)
	{
		mMesh->SetupAttachment(RootComponent);
		mMesh->SetSimulatePhysics(false);
	}

}

// Called when the game starts or when spawned
void APickupItem::BeginPlay()
{
	Super::BeginPlay();

	SetItemTag();
}

void APickupItem::SetItemTag() 
{
	if (mType == EItemType::Coin)
		Tags.Add("Coin");

	if (mType == EItemType::Potion)
		Tags.Add("Potion");
}

// Called every frame
void APickupItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FQuat QuatRotation = FQuat(FRotator(0, 1, 0));
	AddActorLocalRotation(QuatRotation, false, 0, ETeleportType::None);
}


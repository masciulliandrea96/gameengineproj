// Fill out your copyright notice in the Description page of Project Settings.


#include "PlatformObjects/MovingPlatform.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"


AMovingPlatform::AMovingPlatform()
{
	PrimaryActorTick.bCanEverTick = true;

	mRootComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("My Item Root"));
	if (mRootComponent)
	{
		RootComponent = mRootComponent;
	}

	mBoxCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("My Item Sphere"));
	if (mBoxCollider)
	{
		mBoxCollider->SetupAttachment(RootComponent);
	}

	mMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("My Item Mesh"));
	if (mMesh)
	{
		mMesh->SetupAttachment(mBoxCollider);
		mMesh->SetSimulatePhysics(false);
	}

}


void AMovingPlatform::BeginPlay()
{
	Super::BeginPlay();

	// Movement related
	
	mInitialPosition = GetActorLocation();
	mCurrentPosition = mInitialPosition;
	mEndgoalPosition += mInitialPosition;
	mTimeWaitedStill = mTimeToWaitStill;
	mIsMovingBack = false;
	
}


void AMovingPlatform::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	MovePlatform(DeltaTime);
}


// Moves platform back and forth
void AMovingPlatform::MovePlatform(float DeltaTime) 
{
	// do not move if platform has to stand still

	if (mTimeWaitedStill < mTimeToWaitStill) {
		mTimeWaitedStill += DeltaTime;
		return;
	}

	// if platform surpasses end-goal ...

	if ( (mEndgoalPosition - mInitialPosition).SizeSquared() < 
		( mCurrentPosition - mInitialPosition ).SizeSquared() ) 
	{
		// set end-goal as initial-position

		FVector temp = mInitialPosition;
		mInitialPosition = mEndgoalPosition;
		mEndgoalPosition = temp;
		mIsMovingBack ^= true;

		// wait still for a couple of seconds

		mTimeWaitedStill = 0.0f;
	}

	// step towards end-goal

    mCurrentPosition += (mEndgoalPosition - mInitialPosition).GetSafeNormal() * mMovingSpeed * DeltaTime;
	SetActorLocation(mCurrentPosition);
}
#include "Character/EnemyBase.h"

#include "Materials/MaterialInstanceDynamic.h"
#include "Components/SkeletalMeshComponent.h"
#include <GameFramework/Character.h>
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SphereComponent.h"
#include "States/MarioPlayerState.h"
#include "CharacterBaseGameMode.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"


AEnemyBase::AEnemyBase()
{
	PrimaryActorTick.bCanEverTick = true;

	mVulnerableCollider = CreateDefaultSubobject<USphereComponent>(TEXT("Vulnerability Collider"));
	if (mVulnerableCollider)
	{
		mVulnerableCollider->SetupAttachment(RootComponent);
		mVulnerableCollider->SetSimulatePhysics(false);
	}
		
}

void AEnemyBase::BeginPlay()
{
	Super::BeginPlay();

	Tags.Add("Enemy");

	SetInChillMode();

	// Collisions / Overlaps

	if (mVulnerableCollider)
		mVulnerableCollider->OnComponentBeginOverlap.AddDynamic(this, &AEnemyBase::OnHeadOverlapBegin);

	if (GetCapsuleComponent())
		GetCapsuleComponent()->OnComponentHit.AddDynamic(this, &AEnemyBase::OnBodyOverlapBegin);
	
}

void AEnemyBase::SetInChillMode() 
{
	// change color

	UMaterialInstanceDynamic* materialInstance = GetMesh()->CreateDynamicMaterialInstance(0, GetMesh()->GetMaterial(0));
	if (materialInstance)
	{
		materialInstance->SetVectorParameterValue("BodyColor", mMeshColorChill);
		GetMesh()->SetMaterial(0, materialInstance);
	}

	// change speed

	GetCharacterMovement()->MaxWalkSpeed = mSpeedChill;
}

void AEnemyBase::OnHeadOverlapBegin(class UPrimitiveComponent* HitComp, class AActor* OtherActor,
	class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor->ActorHasTag("Player"))
		KillThisEnemy();
}

void AEnemyBase::KillThisEnemy()
{
	// ragdoll

	this->GetCharacterMovement()->Deactivate(); 
	GetMesh()->SetSimulatePhysics(true);
	GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	// destroy w delay

	FTimerHandle UnusedHandle;
	GetWorldTimerManager().SetTimer(UnusedHandle, this, &AEnemyBase::DestroyThisEnemy, 3.0f, false);
}

void AEnemyBase::DestroyThisEnemy() 
{
	this->Destroy(true);
}

void AEnemyBase::OnBodyOverlapBegin(UPrimitiveComponent* HitComponent, AActor* OtherActor, 
	UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor->ActorHasTag("Player"))
		OnPlayerOverlapWithBody();
}

void AEnemyBase::OnPlayerOverlapWithBody() 
{
    AMarioPlayerState* playerstate = Cast<AMarioPlayerState>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0)->GetPlayerState());

	if (playerstate) 
		playerstate->PlayerTakesDamage(1);
}

void AEnemyBase::SetRage(bool isRaging)
{
	mIsRaging = isRaging;
}

void AEnemyBase::SetInRageMode()
{
	// change color

	UMaterialInstanceDynamic* materialInstance = GetMesh()->CreateDynamicMaterialInstance(0, GetMesh()->GetMaterial(0));
	if (materialInstance)
	{
		materialInstance->SetVectorParameterValue("BodyColor", mMeshColorRage);
		GetMesh()->SetMaterial(0, materialInstance);
	}

	// change speed

	GetCharacterMovement()->MaxWalkSpeed = mSpeedRage;
}

void AEnemyBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (mIsRaging) {
		SetInRageMode();
	}
	else {
		SetInChillMode();
	}
}

// Called to bind functionality to input
void AEnemyBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}


// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/BTT_ChasePlayer.h"
#include "Character/EnemyFollowerAIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"

UBTT_ChasePlayer::UBTT_ChasePlayer(FObjectInitializer const& objectInitializer)
{
	NodeName = TEXT("Chase Player");
}

EBTNodeResult::Type UBTT_ChasePlayer::ExecuteTask(UBehaviorTreeComponent& ownerComp, uint8* nodeMemory)
{
	// get TargetLocation from blackboard via AI controller

	AEnemyFollowerAIController* const aiCont = Cast<AEnemyFollowerAIController>(ownerComp.GetAIOwner());
	FVector const playerLocation = aiCont->GetBlackboard()->GetValueAsVector("TargetLocation");

	// move to the player's location

	UAIBlueprintHelperLibrary::SimpleMoveToLocation(aiCont, playerLocation);

	// finish with success

	FinishLatentTask(ownerComp, EBTNodeResult::Succeeded);
	return EBTNodeResult::Succeeded;
}
// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/BTT_Explode.h"
#include "States/MarioPlayerState.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include <Kismet/KismetSystemLibrary.h>
#include <Kismet/KismetMathLibrary.h>
#include "Kismet/GameplayStatics.h"
#include "Camera/CameraShakeBase.h"
#include "Components/CapsuleComponent.h"
#include "Components/StaticMeshComponent.h"
#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Character/EnemyBase.h"

UBTT_Explode::UBTT_Explode(FObjectInitializer const& objectInitializer)
{
	NodeName = TEXT("Explode");
}

EBTNodeResult::Type UBTT_Explode::ExecuteTask(UBehaviorTreeComponent& ownerComp, uint8* nodeMemory)
{
	UE_LOG(LogTemp, Warning, TEXT("Enemy Explosion!!!"));

	// get player and enemy stuff

	ACharacter* const player = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	AMarioPlayerState* playerstate = Cast<AMarioPlayerState>(player->GetPlayerState());
	AEnemyBase* const enemy = Cast<AEnemyBase>(ownerComp.GetAIOwner()->GetCharacter());

	if (!player || !playerstate || !enemy)
	{
		FinishLatentTask(ownerComp, EBTNodeResult::Succeeded);
		return EBTNodeResult::Succeeded;
	}

	// abort if enemy is too distant

	FVector const playerLocation = player->GetActorLocation();
	FVector const thisEnemyLocation = enemy->GetActorLocation();
	FRotator const thisEnemyRotation = enemy->GetActorRotation();

	if (FVector::DistSquared(playerLocation, thisEnemyLocation) > mSearchRadious*mSearchRadious)
	{
		FinishLatentTask(ownerComp, EBTNodeResult::Succeeded);
		return EBTNodeResult::Succeeded;
	}

	// deal damage to player

	if (playerstate)
		playerstate->PlayerTakesDamage(1);

	// spawn emitter & cam-shake

	const FVector EmitterSpawnScale = UKismetMathLibrary::Conv_FloatToVector(FMath::RandRange(7.0f, 8.0f));
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), mExplosionEmitter, thisEnemyLocation, thisEnemyRotation, EmitterSpawnScale, true, EPSCPoolMethod::None, true);
	UGameplayStatics::PlayWorldCameraShake(GetWorld(), mExplosionShake, thisEnemyLocation, .0f, 5000.f, 1.f, false); // TODO: doesn't work

	// kill enemy

	enemy->KillThisEnemy();

	// finish with success

	FinishLatentTask(ownerComp, EBTNodeResult::Succeeded); 
	return EBTNodeResult::Succeeded;
}

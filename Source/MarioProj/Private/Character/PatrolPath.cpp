// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/PatrolPath.h"

// Sets default values
APatrolPath::APatrolPath()
{
	PrimaryActorTick.bCanEverTick = false;
}

FVector APatrolPath::GetPatrolPoints(int const index) const
{
	if (numPatrolPoints() < 1 || index < 0 || index > numPatrolPoints()) // just in case
		return FVector::ZeroVector;

	return patrol_points[index];
}

int APatrolPath::numPatrolPoints() const
{
	return patrol_points.Num();
}



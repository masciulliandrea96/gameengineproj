// Fill out your copyright notice in the Description page of Project Settings.

#include "Character/BTT_FindPlayerLocation.h"
#include "Character/EnemyFollowerAIController.h"

#include "BehaviorTree/BlackboardComponent.h"
#include "Runtime/NavigationSystem/Public/NavigationSystem.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Vector.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "GameFramework/Character.h"

#include "BehaviorTree/BlackboardComponent.h"
#include "Character/EnemyBase.h"


UBTT_FindPlayerLocation::UBTT_FindPlayerLocation(FObjectInitializer const& object_initializer)
{
	NodeName = TEXT("Find Player Location");
}

EBTNodeResult::Type UBTT_FindPlayerLocation::ExecuteTask(UBehaviorTreeComponent& ownerBehTreeComp, uint8* nodeMemory)
{
	// get player character and the NPC's controller

	ACharacter* const player = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	auto const enemyController = Cast<AEnemyFollowerAIController>(ownerBehTreeComp.GetAIOwner());

	// get player location to use as an origin

	FVector const playerLocation = player->GetActorLocation();

	mSearchRandom = false;

	if (mSearchRandom)
	{
		FNavLocation loc;

		// get random location near the player (from navigation system)

		UNavigationSystemV1* const navSys = UNavigationSystemV1::GetCurrent(GetWorld());

		if (navSys->GetRandomPointInNavigableRadius(playerLocation, mSearchRadius, loc, nullptr))
		{
			enemyController->GetBlackboard()->SetValueAsVector("TargetLocation", loc.Location);
		}
	}
	else
	{
		enemyController->GetBlackboard()->SetValueAsVector("TargetLocation", playerLocation);
	}


	// finish with success

	FinishLatentTask(ownerBehTreeComp, EBTNodeResult::Succeeded);
	return EBTNodeResult::Succeeded;
}
// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/EnemyAIController.h"

#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "UObject/ConstructorHelpers.h"

#include "Character/EnemyBase.h"

AEnemyAIController::AEnemyAIController(FObjectInitializer const& objectInitializer /*= FObjectInitializer::Get()*/)
{
	mBehaviorTreeComponent = objectInitializer.CreateDefaultSubobject<UBehaviorTreeComponent>(this, TEXT("BehaviorComp"));
	mBlackboardComponent = objectInitializer.CreateDefaultSubobject<UBlackboardComponent>(this, TEXT("BlackboardComp"));
}

void AEnemyAIController::BeginPlay()
{
	Super::BeginPlay();
}

void AEnemyAIController::OnPossess(APawn* const pawn)
{
	Super::OnPossess(pawn);

	AEnemyBase* enemyBase = Cast<AEnemyBase>(pawn);

	if (enemyBase) 
	{
		mBehaviorTree = enemyBase->mBehaviourTree;

		if (mBehaviorTree)
		{
			RunBehaviorTree(mBehaviorTree);
			mBehaviorTreeComponent->StartTree(*mBehaviorTree);
		}

	}

	if (mBehaviorTree != nullptr && mBehaviorTreeComponent != nullptr && mBlackboardComponent != nullptr) 
	{
		mBlackboardComponent->InitializeBlackboard(*mBehaviorTree->BlackboardAsset);
	}
}

class UBlackboardComponent* AEnemyAIController::GetBlackboard() const
{
	return mBlackboardComponent;
}

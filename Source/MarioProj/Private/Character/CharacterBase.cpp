// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/CharacterBase.h"

#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"


#include "Materials/MaterialInstanceDynamic.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/PlayerController.h"
#include "Perception/AIPerceptionStimuliSourceComponent.h"
#include "Perception/AISense_Sight.h"

#include "States/MarioPlayerState.h"
#include "CharacterBaseGameMode.h"


// Sets default values
ACharacterBase::ACharacterBase()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.

	PrimaryActorTick.bCanEverTick = true;

	// Collision

	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);
	GetCapsuleComponent()->OnComponentBeginOverlap.AddDynamic(this, &ACharacterBase::OnOverlapBegin);

	// set our turn rates for input

	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.

	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement

	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.8f;

	// Create a camera boom (pulls in towards the player if there is a collision)

	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller
	CameraBoom->bDoCollisionTest = false;

	// Create a follow camera

	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Setup perception

	setupStimulus();

}

void ACharacterBase::OnExitGame()
{
	GetWorld()->GetFirstPlayerController()->ConsoleCommand("quit");
}

void ACharacterBase::setupStimulus()
{
	stimulus = CreateDefaultSubobject<UAIPerceptionStimuliSourceComponent>(TEXT("stimulus"));
	stimulus->RegisterForSense(TSubclassOf<UAISense_Sight>());
	stimulus->RegisterWithPerceptionSystem();
}

// Called when the game starts or when spawned
void ACharacterBase::BeginPlay()
{
	Super::BeginPlay();

	// limit pitch of camera

	APlayerCameraManager* const cameraManager = GetWorld()->GetFirstPlayerController()->PlayerCameraManager;
	cameraManager->ViewPitchMin = -50.0f;
	cameraManager->ViewPitchMax = 30.0f;

	// change color

	// UMaterialInstanceDynamic* const materialInstance = UMaterialInstanceDynamic::Create(GetMesh()->GetMaterial(0), this);
	UMaterialInstanceDynamic* materialInstance = GetMesh()->CreateDynamicMaterialInstance(0, GetMesh()->GetMaterial(0));
	if (materialInstance)
	{
		materialInstance->SetVectorParameterValue("BodyColor", FLinearColor::Green);
		GetMesh()->SetMaterial(0, materialInstance);
	}

	// other

	isDead = false;

	Tags.Add("Player");
}

// Called every frame
void ACharacterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ACharacterBase::OnOverlapBegin(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, 
	int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor->ActorHasTag("Coin")) 
	{
		OnCoinOverlap();
		OtherActor->Destroy();
	}

	if (OtherActor->ActorHasTag("Potion")) 
	{
		OnPotionOverlap();
		OtherActor->Destroy();
	}
}

void ACharacterBase::OnCoinOverlap()
{
	UE_LOG(LogTemp, Warning, TEXT("Player Collided With Coin"));

	AMarioPlayerState* characterBaseState = Cast<AMarioPlayerState>(GetPlayerState());

	if (characterBaseState) {
		characterBaseState->AddCoins(1);
	}
}

void ACharacterBase::OnPotionOverlap()
{
	UE_LOG(LogTemp, Warning, TEXT("Player Collided With Potion"));

	AMarioPlayerState* characterBaseState = Cast<AMarioPlayerState>(GetPlayerState());

	if (characterBaseState) {
		characterBaseState->RestoreHearts();
	}
}

void ACharacterBase::CharacterStartBlink()
{
	// change color

	UMaterialInstanceDynamic* materialInstance = GetMesh()->CreateDynamicMaterialInstance(0, GetMesh()->GetMaterial(0));
	if (materialInstance)
	{
		materialInstance->SetVectorParameterValue("BodyColor", FLinearColor(0.2f, 0.3f, 0.2f)); // Darker green
		GetMesh()->SetMaterial(0, materialInstance);
	}
}

void ACharacterBase::CharacterStopBlink()
{
	// change color

	UMaterialInstanceDynamic* materialInstance = GetMesh()->CreateDynamicMaterialInstance(0, GetMesh()->GetMaterial(0));
	if (materialInstance)
	{
		materialInstance->SetVectorParameterValue("BodyColor", FLinearColor(0.0f, 1.0f, 0.0f)); // Green
		GetMesh()->SetMaterial(0, materialInstance);
	}
}

void ACharacterBase::CharacterDeath()
{
	if (isDead)
		return;

	isDead = true;

	GetMesh()->SetSimulatePhysics(true); // ragdoll
	GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
	GetCharacterMovement()->Deactivate();
	GetCameraBoom()->Deactivate();

	ACharacterBaseGameMode* gameMode = Cast<ACharacterBaseGameMode>(GetWorld()->GetAuthGameMode());

	if (gameMode)
		gameMode->RestartGame();

}

void ACharacterBase::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ACharacterBase::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ACharacterBase::MoveForward(float Value)
{
	if ((Controller != nullptr) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void ACharacterBase::MoveRight(float Value)
{
	if ((Controller != nullptr) && (Value != 0.0f))
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

// Called to bind functionality to input
void ACharacterBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &ACharacterBase::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ACharacterBase::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ACharacterBase::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ACharacterBase::LookUpAtRate);

	// exit game functionality
	PlayerInputComponent->BindAction("ExitGame", IE_Pressed, this, &ACharacterBase::OnExitGame);

}

void ACharacterBase::FellOutOfWorld(const UDamageType& dmgType)
{
	ACharacterBaseGameMode* gameMode = Cast<ACharacterBaseGameMode>(GetWorld()->GetAuthGameMode());

	if (gameMode)
		gameMode->RestartGame();
}


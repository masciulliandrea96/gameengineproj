// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/EnemyFollowerAIController.h"
#include "Character/CharacterBase.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Character/EnemyBase.h"
#include "Perception/AISenseConfig_Sight.h"
#include "Perception/AIPerceptionStimuliSourceComponent.h"
#include "Perception/AIPerceptionComponent.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "GameFramework/Character.h"

AEnemyFollowerAIController::AEnemyFollowerAIController(FObjectInitializer const& objectInitializer /*= FObjectInitializer::Get()*/)
{
	mBehaviorTreeComponent = objectInitializer.CreateDefaultSubobject<UBehaviorTreeComponent>(this, TEXT("BehaviorComp"));
	mBlackboardComponent = objectInitializer.CreateDefaultSubobject<UBlackboardComponent>(this, TEXT("BlackboardComp"));

	SetupPerceptionSystem();
}

void AEnemyFollowerAIController::BeginPlay()
{
	Super::BeginPlay();
}

void AEnemyFollowerAIController::OnPossess(APawn* const pawn)
{
	Super::OnPossess(pawn);

	mEnemyPawnControlled = Cast<AEnemyBase>(pawn);

	if (mEnemyPawnControlled)
	{
		mBehaviorTree = mEnemyPawnControlled->mBehaviourTree;

		if (mBehaviorTree)
		{
			RunBehaviorTree(mBehaviorTree);
			mBehaviorTreeComponent->StartTree(*mBehaviorTree);
		}

	}

	if (mBehaviorTree != nullptr && mBehaviorTreeComponent != nullptr && mBlackboardComponent != nullptr)
	{
		mBlackboardComponent->InitializeBlackboard(*mBehaviorTree->BlackboardAsset);
	}
}

class UBlackboardComponent* AEnemyFollowerAIController::GetBlackboard() const
{
	return mBlackboardComponent;
}

void AEnemyFollowerAIController::onTargetDetected(AActor* actor, FAIStimulus const stimulus)
{
	if (auto const ch = Cast<ACharacterBase>(actor)) 
	{
		GetBlackboard()->SetValueAsBool("CanSeePlayer", stimulus.WasSuccessfullySensed());

		if (!mEnemyPawnControlled)
			return;

		// enemy behavior when player under enemy vision

		if (stimulus.WasSuccessfullySensed()) 
		{
			mEnemyPawnControlled->SetRage(true);
		}
		else 
		{
			mEnemyPawnControlled->SetRage(false);
		}
	}
}

void AEnemyFollowerAIController::SetupPerceptionSystem()
{
	// create and initialize sight configuration object
	mSightConfig = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("Sight Config"));
	if (mSightConfig)
	{
		SetPerceptionComponent(*CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("Perception Component")));
		mSightConfig->SightRadius = 1000.0f;
		mSightConfig->LoseSightRadius = mSightConfig->SightRadius + 200.0f;
		mSightConfig->PeripheralVisionAngleDegrees = 110.0f; // angle of view
		mSightConfig->SetMaxAge(0.0f); // forgets of following
		mSightConfig->AutoSuccessRangeFromLastSeenLocation = 500.0f;
		mSightConfig->DetectionByAffiliation.bDetectEnemies = true;
		mSightConfig->DetectionByAffiliation.bDetectFriendlies = true;
		mSightConfig->DetectionByAffiliation.bDetectNeutrals = true;

		// add sight configuration component to perception component
		GetPerceptionComponent()->SetDominantSense(*mSightConfig->GetSenseImplementation());
		GetPerceptionComponent()->OnTargetPerceptionUpdated.AddDynamic(this, &AEnemyFollowerAIController::onTargetDetected);
		GetPerceptionComponent()->ConfigureSense(*mSightConfig);
	}
}

void AEnemyFollowerAIController::OnUpdated(TArray<AActor*> const& updatedActors)
{
	// Iterates for all actors sensed. Not needed for now...
	// for (size_t x = 0; x < updatedActors.Num(); ++x){}
}

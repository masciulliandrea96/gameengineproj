#include "Character/BTT_FindRandomLocation.h"

#include "BehaviorTree/BlackboardComponent.h"
#include "Runtime/NavigationSystem/Public/NavigationSystem.h"
#include "Character/EnemyAIController.h"
#include "Character/EnemyFollowerAIController.h"
//#include "BehaviorTree/Blackboard/BlackboardKeyType.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Vector.h"

UBTT_FindRandomLocation::UBTT_FindRandomLocation(FObjectInitializer const& object_initializer)
{
	NodeName = TEXT("Find Random Location");
}

EBTNodeResult::Type UBTT_FindRandomLocation::ExecuteTask(UBehaviorTreeComponent& ownerComp, uint8* nodeMemory)
{
	// get AI controller and its Enemy

	auto const aiController = Cast<AEnemyFollowerAIController>(ownerComp.GetAIOwner());

	if (aiController == nullptr) 
	{
		FinishLatentTask(ownerComp, EBTNodeResult::Failed);
		return EBTNodeResult::Failed;
	}

	auto const enemy = aiController->GetPawn();

	// obtain enemy location to use as origin location

	FVector const origin = enemy->GetActorLocation();

	// get the navigation system and generate a random location on the NavMesh

	UNavigationSystemV1* const nav_sys = UNavigationSystemV1::GetCurrent(GetWorld());
	FNavLocation loc;

	if (nav_sys->GetRandomPointInNavigableRadius(origin, searchRadious, loc, nullptr)) 
	{
		aiController->GetBlackboard()->SetValueAsVector("TargetLocation", loc.Location);
	}

	// finish with success

	FinishLatentTask(ownerComp, EBTNodeResult::Succeeded);
	return EBTNodeResult::Succeeded;
}

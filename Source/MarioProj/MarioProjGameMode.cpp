// Copyright Epic Games, Inc. All Rights Reserved.

#include "MarioProjGameMode.h"
#include "MarioProjCharacter.h"
#include "UObject/ConstructorHelpers.h"

AMarioProjGameMode::AMarioProjGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Image.h"
#include "States/MarioPlayerState.h"
#include "PlayerHUDWidget.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class MARIOPROJ_API UPlayerHUDWidget : public UUserWidget
{
	GENERATED_BODY()

protected:

	UFUNCTION(BlueprintCallable)
	void UpdateCoins(int newCoinAmount);

	UFUNCTION(BlueprintCallable)
	void UpdateLives(int newLifeAmount);

	void NativeOnInitialized() override;
	
public:

	UPROPERTY(meta = (BindWidget, DisplayName = "Coins Amount Text"))
	class UTextBlock* mCoinsText;

	UPROPERTY(meta = (BindWidget, DisplayName = "Heart Image 1"))
	class UImage* mHeartImage_1;

	UPROPERTY(meta = (BindWidget, DisplayName = "Heart Image 2"))
	class UImage* mHeartImage_2;

	UPROPERTY(meta = (BindWidget, DisplayName = "Heart Image 3"))
	class UImage* mHeartImage_3;

protected:

	UPROPERTY()
	class AMarioPlayerState* mPlayerState;

};

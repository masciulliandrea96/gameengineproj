// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MovingPlatform.generated.h"

class UStaticMeshComponent;
class UBoxComponent;

UCLASS()
class MARIOPROJ_API AMovingPlatform : public AActor
{
	GENERATED_BODY()
	
public:	
	
	AMovingPlatform();

	// Mesh and Colliders

	UPROPERTY(Category = "My Meshes", EditDefaultsOnly, BlueprintReadWrite)
	UStaticMeshComponent* mMesh;

	UPROPERTY(Category = "My Colliders", EditDefaultsOnly, BlueprintReadWrite)
	UBoxComponent* mBoxCollider;

	// Movement Related

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "My Movement", meta = (MakeEditWidget = "true", AllowPrivateAccess = "true"))
	float mMovingSpeed = 120;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "My Movement", meta = (MakeEditWidget = "true", AllowPrivateAccess = "true"))
	FVector mEndgoalPosition;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "My Movement", meta = (MakeEditWidget = "true", AllowPrivateAccess = "true"))
	float mTimeToWaitStill = 4;

private:

	// Movement Related

	FVector mInitialPosition;
	FVector mCurrentPosition;
	float mTimeWaitedStill;
	bool mIsMovingBack;

	// Root component

	USceneComponent* mRootComponent;

public:	

	virtual void Tick(float DeltaTime) override;

protected:

	virtual void BeginPlay() override;

private:

	void MovePlatform(float DeltaTime);
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PickupItem.generated.h"

class UStaticMeshComponent;
class USphereComponent;

UENUM(BlueprintType)
enum class EItemType : uint8 {
	Coin,
	Potion
};

UCLASS()
class MARIOPROJ_API APickupItem : public AActor
{
	GENERATED_BODY()

public:
	
	APickupItem();

	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void SetItemTag();

protected:
	
	virtual void BeginPlay() override;

public:
	
	UPROPERTY(Category = "MarioProj : Meshes", EditDefaultsOnly, BlueprintReadWrite)
	UStaticMeshComponent* mMesh;

	UPROPERTY(Category = "MarioProj : Components", EditDefaultsOnly, BlueprintReadWrite)
	USphereComponent* mSphere;

	UPROPERTY(Category = "Drop", EditAnywhere, BlueprintReadWrite)
	int Amount = 1;

	UPROPERTY(Category = "Drop", EditAnywhere, BlueprintReadWrite)
	EItemType mType;



};

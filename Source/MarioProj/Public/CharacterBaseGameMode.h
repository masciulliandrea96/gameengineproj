// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "CharacterBaseGameMode.generated.h"

/**
 * 
 */
UCLASS()
class MARIOPROJ_API ACharacterBaseGameMode : public AGameMode
{
	GENERATED_BODY()

public:

	ACharacterBaseGameMode();

	void ReopenLevel();

	void RestartGame();

	void WinLevel();

};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "MarioPlayerState.generated.h"

/**
 * 
 */

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCoinAmountUpdate, int, newCoinAmount);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FLifeAmountUpdate, int, newLifeAmount);

UCLASS()
class MARIOPROJ_API AMarioPlayerState : public APlayerState
{
	GENERATED_BODY()

public:

	AMarioPlayerState();

	UFUNCTION()
	void AddCoins(int coins = 1);

	UFUNCTION()
	void PlayerTakesDamage(int damage);

	UFUNCTION()
	void RestoreHearts();

	UFUNCTION()
	void PlayerDeath();

private:

	UFUNCTION()
	void RemoveHeart(int damage = 1);

	UFUNCTION()
	void SetVincible();

	UFUNCTION()
	void SetInvincible();
	
public:

	FCoinAmountUpdate OnCoinAmountUpdate;
	FLifeAmountUpdate OnLifeAmountUpdate;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Coins")
	int mCoins;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Coins")
	int mCoinsToWin = 25; // consider using delegate to update coins-to-win UI

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Hearts")
	int mMaxNumHearts = 3;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Hearts")
	int mCurrentHearts;

private:

	bool mIsInvincible = false;

};

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Perception/AIPerceptionTypes.h"
#include "EnemyFollowerAIController.generated.h"

class AEnemyBase;

/**
 *
 */
UCLASS()
class MARIOPROJ_API AEnemyFollowerAIController : public AAIController
{
	GENERATED_BODY()

public:

	AEnemyFollowerAIController(FObjectInitializer const& objectInitializer = FObjectInitializer::Get());

	void BeginPlay() override;

	void OnPossess(APawn* const pawn) override;

	class UBlackboardComponent* GetBlackboard() const;

	UFUNCTION()
	void OnUpdated(TArray<AActor*> const& updatedActors);

	UFUNCTION()
	void onTargetDetected(AActor* actor, FAIStimulus const stimulus);

	void SetupPerceptionSystem();

private:

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category = "AI", meta = (AllowPrivateAccess = "true"))
	class UBehaviorTreeComponent* mBehaviorTreeComponent;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category = "AI", meta = (AllowPrivateAccess = "true"))
	class UBehaviorTree* mBehaviorTree;

	class UBlackboardComponent* mBlackboardComponent;

	class UAISenseConfig_Sight* mSightConfig;

	class AEnemyBase* mEnemyPawnControlled;


};

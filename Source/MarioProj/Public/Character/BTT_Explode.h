// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "BehaviorTree/BehaviorTreeTypes.h"
#include "BTT_Explode.generated.h"


class UParticleSystem;
class UCameraShakeBase;

/**
 * 
 */
UCLASS(Blueprintable)
class MARIOPROJ_API UBTT_Explode : public UBTTask_BlackboardBase
{
	GENERATED_BODY()
	
public:

	UBTT_Explode(FObjectInitializer const& objectInitializer);

	EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& ownerComp, uint8* nodeMemory);

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explosion", meta = (AllowPrivateAccess = "true"))
	float mSearchRadious = 800.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explosion", meta = (AllowPrivateAccess = "true"))
	float mExplosionRadious = 500.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explosion", meta = (AllowPrivateAccess = "true"))
	UParticleSystem* mExplosionEmitter;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explosion", meta = (AllowPrivateAccess = "true"))
	TSubclassOf<UCameraShakeBase> mExplosionShake;

};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Components/SphereComponent.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "EnemyBase.generated.h"

UCLASS()
class MARIOPROJ_API AEnemyBase : public ACharacter
{
	GENERATED_BODY()

public:

	AEnemyBase();

	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
	void OnHeadOverlapBegin(class UPrimitiveComponent* HitComp, class AActor* OtherActor, 
		class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnBodyOverlapBegin(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
		FVector NormalImpulse, const FHitResult& Hit);

	UFUNCTION()
	void KillThisEnemy();

	UFUNCTION()
	void SetRage(bool isRaging);

protected:

	virtual void BeginPlay() override;

private:

	void OnPlayerOverlapWithBody();

	void SetInRageMode();

	void SetInChillMode();

	void DestroyThisEnemy();

public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "My Mesh and Colliders")
	class USphereComponent* mVulnerableCollider;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "My AI Behaviour", meta = (AllowPrivateAccess = "true"))
	class UBehaviorTree* mBehaviourTree;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "My AI Behaviour", meta = (AllowPrivateAccess = "true"))
	FLinearColor mMeshColorChill = FLinearColor::Yellow;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "My AI Behaviour", meta = (AllowPrivateAccess = "true"))
	FLinearColor mMeshColorRage = FLinearColor::Red;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "My AI Behaviour", meta = (AllowPrivateAccess = "true"))
	float mSpeedChill = 150.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "My AI Behaviour", meta = (AllowPrivateAccess = "true"))
	float mSpeedRage = 400.0f;

private:

	bool mIsRaging = false;

};

// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MarioProjGameMode.generated.h"

UCLASS(minimalapi)
class AMarioProjGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMarioProjGameMode();
};




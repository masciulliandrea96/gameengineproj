# Mario Project 64

![peak-marioproj](https://user-images.githubusercontent.com/32450751/236648548-2da2d785-a1b3-4944-a192-20133df921bb.gif)

SuperMario 64 inspired platformer, developed in Unreal Engine 4 as a university project for the Game Engine and Tools Programming courses.

- **Title** : Mario Project 64
- **Genre** : Platformer
- **Developer** : Andrea Masciulli
- **Project stage** : Published
- **UE Version** : 4.27
- **IDE** : Visual Studio 2022
- **Gameplay video** : https://youtu.be/eCsJzPGRW0c
- **Build on Itch** : https://aramas.itch.io/unreal-engine-platformer-game-engine-assignment

## Gameplay

Player must pick up all coins dispersed in the level. Falling or losing all lives resets the level. Platforms and Enemies make the experience challenging.

## Features

This is a prototype platformer. Basic gameplay features which have been implemented:
- **Playable Level** : A level where the player controlled pawn can move and jump to reach all different level sections.
- **Platforms** : Moving platform to jump onto.
- **Enemies** : Enemies wonder around the level, chasing the player when seen. Jump on them to defeat them.
- **Items** : Coins and Potions to collect. Game is won when all coins are picked up. Potions heal damages dealt by enemies.
- **Menus** : Simple start pause and end game menus are present.
- **HUD** : A minimal HUD displays player health and coins picked up vs coins left.
- **C++** : Most game logic (except UI elements) have been implemented in C++

